import { useEffect, useState } from "react"

export const useForm = (initialState) => {
    const [data, setData] = useState(initialState);
    const [error, setError] = useState(initialState);
    const [isError, setIsError] = useState(false);

    const validation = (name, value) => {
        switch (name) {
            case 'username':
                if (!/^[a-zA-Z0-9_-]{4,15}$/.test(value)) {
                    setError((currentData) => {
                        return {
                            ...currentData,
                            [name]: `${name} neut respect pas le critaire`
                        }
                    })
                } else {
                    setError((currentData) => ({
                        ...currentData,
                        [name]: ""
                    })
                    )
                }
                break;
            case 'password':
                if (!/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,64}$/.test(value)) {
                    setError((currentData) => {
                        return {
                            ...currentData,
                            [name]: `${name} neut respect pas le critaire`
                        }
                    })
                } else {
                    setError((currentData) => ({
                        ...currentData,
                        [name]: ""
                    })
                    )
                }
                break;
            default:
                break;
        }
    }


    useEffect(() => {
        let isErr = false

        Object.values(error).forEach((value) => {
            console.log(isErr, " ", value)
            if (value != '') {
                isErr = true;
            }
        })
        console.log(isErr)
        setIsError(isErr);
    }, [error])

    const fromHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setData((currentData) => {
            return {
                ...currentData,
                [name]: value
            }
        })

        validation(name, value)
    }

    return { data, error, isError, fromHandler }
}