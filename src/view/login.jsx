import { useEffect } from "react"
import { InputLogin } from "../components/inputLogin"
import { useForm } from "../hooks/useForm"

export const Login = () => {
    const { data, error, isError, fromHandler } = useForm({ username: "", password: "" })
    useEffect(() => {
        console.log("data : ", data)
        console.log("isError : ", isError)
    }, [data])
    return (
        <div>
            <InputLogin titre={"username"} type="text" onChange={fromHandler} error={error.username} />
            <InputLogin titre={"password"} type="password" onChange={fromHandler} error={error.password} />
        </div>
    )
}