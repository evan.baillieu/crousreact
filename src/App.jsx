import { Route, Routes } from 'react-router-dom'
import './App.css'
import { Login } from './view/login'

export function App() {

  return (
    <div>
      <Routes>
        <Route path="/login" element={<Login />} />
      </Routes>
    </div >
  )
}


