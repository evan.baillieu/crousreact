export const InputLogin = ({ titre, type, error, onChange }) => {
    return (

        <div>
            <label htmlFor="">{titre}</label>
            <input type={type} name={titre} id={titre} onChange={onChange} />
            <span>{error}</span>
        </div>
    )
}